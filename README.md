<img alt="GarudaLinux" src="./834_2.png" />

# Garuda Linux

This is source code of Garuda Linux website found at [https://garudalinux.org/](https://garudalinux.org/).

## Table of Contents

- [How to contribute?](#how-to-contribute)
- [Report a bug](#report-a-bug)
- [Repo Maintainers](#maintainers)
- [Deployment](#deployment)
- [Contact](#contact)

## How to Contribute?

You can browse through existing issues or open a new issue  and submit a PR to fix that issue.
For detailed information read our [contribution guidelines](./CONTRIBUTING.md)

## Report a bug

If you encountered an issue with the website, check if it has already been reported. If not, check [Reporting Bugs](https://wiki.garudalinux.org/en/reporting-bugs).

## Maintainers

- @SGSm
- @dal.delmonico
- @dr460nf1r3
- @Yorper
- @Namanlp
- @RohitSingh107
- @JustTNE
- @jonathon (rest in peace, friend!)
- @PedroHLC
- @petsam
- @dalto.8
- @zoeruda
- @Edu4rdSHL

## Deployment

A simple CI/CD pipeline has been set up to deploy this website to Cloudflare pages. It checks HTML, CSS and JS against common errors and pushes the `master` branch contents to the `cf-pages` branch, which is then automatically published to Cloudflare pages.

## Contact

Contact us via [mail](mailto:team@garudalinux.org) or the [forum](https://forum.garudalinux.org/).
